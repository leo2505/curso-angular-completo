export interface Restaurant {
    id: string
    name: string
    deliveryEstimate: string
    rating: number
    imagePath: string
    category: string
    hours?: string
    about?: string
}