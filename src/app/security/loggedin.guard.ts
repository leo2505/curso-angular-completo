import { LoginService } from './login/login.service';
import { Injectable } from '@angular/core';
import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterState, RouterStateSnapshot } from "@angular/router";


@Injectable()
export class LoggedInGuard implements CanLoad, CanActivate {
    constructor(
        private loginService: LoginService
    ) { }
    checkAuthentication(path: string): boolean {
        const logedIn = this.loginService.isLoggedIn()
        if (!logedIn) {
            this.loginService.handleLogin(`/${path}`)
        }
        return logedIn
    }
    canLoad(route: Route): boolean {
        return this.checkAuthentication(route.path)
    }
    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
        return this.checkAuthentication(activatedRoute.routeConfig.path)
    }
}